sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/ui/core/HTML",
    "./dependencies/qr/zxing",
    "./dependencies/qr/quagga.min"
], function (Controller, Dialog, Button, HTML, _1D, _2D) {
    return {
        onDetected: function(e, input) {
            let mean;
            let decodedCodes = e.codeResult.decodedCodes;
            for (let decodedCode of decodedCodes) {
                mean ? mean += decodedCode.error : mean = decodedCode.error;
            }
            mean = mean/decodedCodes.length;
            if (mean < 0.1) {
                input.setValue(e.codeResult.code);
                this.oDialog_QR1.close();
                Quagga.stop();
                this.oZX_I1.reset();
            }
        },
        popQuagga: function(input) {
            Quagga.init(this.getQuaggaConfig(), (error) => {
                if (error) {
                    console.error(error);
                    return
                }
                Quagga.start();
            });
            Quagga.onProcessed(this.onProcessed.bind(this));
            Quagga.onDetected((e) => {
                this.onDetected(e, input);
            });
        },
        popZXing: function(input) {
            let target = document.querySelector("#idQuagga video");
            this.oZX_I1 = new ZXing.BrowserQRCodeReader();
            this.oZX_I1.decodeOnceFromVideoDevice(undefined, target).then(data => {
                input.setValue(data);
                this.oDialog_QR1.close();
                Quagga.stop();
                this.oZX_I1.reset();
            }).catch(error => {
                console.error(error);
            })
        },
        popQR: function(input) {
            if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
                this.popQuagga(input);
                this.popZXing(input);
            } else {
                MessageToast.show("Browser does not support Camera API.")
            }
        },
        getQRDialog: function(oEvent, field) {
            let target = field ? field : oEvent.getSource();
            if (!this.oDialog_QR1) {
				this.oDialog_QR1 = new Dialog({
					title: "Scan QR",
					content: new HTML({
                        content: `<div id="idQuagga"></div>`
                    }),
					endButton: new Button({
						text: "Close",
						press:  () => {
                            this.oDialog_QR1.close();
                            Quagga.stop();
                            this.oZX_I1.reset();
                        }
					})
				}).addStyleClass("dxcUiDialog");
			}
            this.oDialog_QR1.open();
            this.popQR(target);
        },
        getQuaggaConfig: function() {
            let target = document.getElementById("idQuagga");
            return {
                inputStream: {
                    type : "LiveStream",
                    constraints: {
                        width: {
                            min: 640
                        },
                        height: {
                            min: 480
                        },
                        facingMode: "environment",
                        aspectRatio: {
                            min: 1, 
                            max: 2
                        }
                    },
                    target: target
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: 2,
                frequency: 10,
                decoder: {
                    readers : ["code_128_reader", "code_39_reader", "code_39_vin_reader"],
                    multiple: false,
                    debug: {
                        drawBoundingBox: true,
                        showFrequency: true,
                        drawScanline: true,
                        showPattern: true
                    }
                },
                locate: true
            };
        },
        onProcessed: function(e) {
            var context = Quagga.canvas.ctx.overlay, canvas = Quagga.canvas.dom.overlay;
            if (e) {
                if (e.boxes) {
                    context.clearRect(0, 0, parseInt(canvas.getAttribute("width")), parseInt(canvas.getAttribute("height")));
                    e.boxes.filter((box) => {
                        return box !== e.box;
                    }).forEach((box) => {
                        Quagga.ImageDebug.drawPath(
                            box, 
                            {
                                x: 0, 
                                y: 1
                            }, 
                            context, 
                            {
                                color: "#F2F2F2", 
                                lineWidth: 2
                            });
                    });
                }
                if (e.box) {
                    Quagga.ImageDebug.drawPath(
                        e.box, 
                        {
                            x: 0, 
                            y: 1
                        }, 
                        context, 
                        {
                            color: "#00A651", 
                            lineWidth: 2
                        }
                    );
                }
                if (e.codeResult && e.codeResult.code) {
                    Quagga.ImageDebug.drawPath(e.line, 
                        {
                            x: 'x', 
                            y: 'y'
                        }, 
                        context, 
                        {
                            color: '#00A651', 
                            lineWidth: 2
                        }
                    );
                }
            }
        },
    }
});