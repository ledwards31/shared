/* eslint-disable semi */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
// eslint-disable-next-line
sap.ui.define(["sap/ui/model/json/JSONModel"], function (JSONModel) {
  class Filter {
    constructor(filter) {
      if (filter.aFilters) {
        this.bAnd = filter.bAnd;
        this.iterable = filter.aFilters.map(e => {
          return {
            sOperator: e.sOperator,
            oValue1: e.oValue1,
            oValue2: e.oValue2,
            sPath: e.sPath
          }
        });
      } else if (filter.filters) {
        this.bAnd = filter.and;
        this.iterable = filter.filters.map(e => {
          return {
            sOperator: e.sOperator,
            oValue1: e.oValue1,
            oValue2: e.oValue2,
            sPath: e.sPath
          }
        });
      } else if (typeof filter === "object") {
        this.iterable = [{
          sOperator: filter.sOperator,
          oValue1: filter.oValue1,
          oValue2: filter.oValue2,
          sPath: filter.sPath
        }]
      } else {
        throw new Error("Illegal Argument: Argument filter must equal typeof sap.ui.model.Filter.");
      }
      this.fOperator = {
        BT: (value, filter) => {
          return value[filter.sPath] >= filter.oValue1 && value[filter.sPath] <= filter.oValue2;
        },
        Contains: (value, filter) => {
          return value[filter.sPath].includes(filter.oValue1);
        },
        EndsWith: (value, filter) => {
          let exp = new RegExp(`${value[filter.sPath]}$`, 'i');
          return exp.test(filter.oValue1);
        },
        EQ: (value, filter) => {
          return value[filter.sPath] == filter.oValue1;
        },
        GE: (value, filter) => {
          return value[filter.sPath] >= filter.oValue1;
        },
        GT: (value, filter) => {
          return value[filter.sPath] > filter.oValue1;
        },
        LE: (value, filter) => {
          return value[filter.sPath] <= filter.oValue1;
        },
        LT: (value, filter) => {
          return value[filter.sPath] < filter.oValue1;
        },
        NB: (value, filter) => {
          return value[filter.sPath] < filter.oValue1 && value[filter.sPath] > filter.oValue2;
        },
        NE: (value, filter) => {
          return value[filter.sPath] != filter.oValue1;
        },
        NotContains: (value, filter) => {
          return !value[filter.sPath].includes(filter.oValue1);
        },
        NotEndsWith: (value, filter) => {
          let exp = new RegExp(`${value[filter.sPath]}$`, 'i');
          return !exp.test(filter.oValue1);
        },
        StartsWith: (value, filter) => {
          let exp = new RegExp(`^${value[filter.sPath]}`, 'i');
          return exp.test(filter.oValue1);
        },
        NotStartsWith: (value, filter) => {
          let exp = new RegExp(`^${value[filter.sPath]}`, 'i');
          return !exp.test(filter.oValue1);
        },
        _Escape: (e) => {
          return e.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        }
      };
    }
    test(value) {
      let method = this.bAnd ? "every" : "some";
      let response = this.iterable[method](filter => {
        return this.fOperator[filter.sOperator](value, filter);
      });
      console.log(response);
      return response;
    }
  }
  class IndexedDB {
    constructor() {
      this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      this.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || this.msIDBTransaction;
      this.IDBKeyRange = window.IDBKeyRange || this.webkitIDBKeyRange || window.msIDBKeyRange
      this.stores = [
        "Downloads", 
        "Pending", 
        "Files", 
        "Orders", 
        "Config", 
        "OrdersSAP", 
        "Request", 
        "_Request", 
        "__Request", 
        "_Attachments", 
        "_Fetch", 
        "Users", 
        "UserMeta",
        "OrdersList",
        "Batch",
        "DisplayConfig",
        "UserPreferences",
        "Details"
      ];
      if (!this.indexedDB) {
        console.error("Your browser doesn't support a stable version of IndexedDB.")
      } else {
        let request = this.indexedDB.open("IRIS", 1);
        request.onerror = (e) => {
          console.error(e.target.error)
        }
        request.onsuccess = (e) => {
          let db = request.result;
        }
        request.onupgradeneeded = (e) => {
          let db = request.result;
          this.stores.forEach(store => {
            db.createObjectStore(store, {
              keyPath: "id"
            });
          });
        }
      }
    }
    ReadSome(table, filter) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        let items = [];
        try {
          if (filter) filter = new Filter(filter);
        } catch (error) {
          reject(error);
        }
        request.onsuccess = (e) => {
          let db = request.result;
          let store = db.transaction(table).objectStore(table);
          let _cursor = store.openCursor();
          _cursor.onsuccess = (e) => {
            var cursor = e.target.result;
            if (cursor) {
              if (filter) {
                if (filter.test(cursor.value)) items.push(cursor.value);
              } else {
                items.push(cursor.value);
              }
              cursor.continue();
            } else {
              resolve(items);
            }
          }
          _cursor.onerror = (e) => {
            reject(e);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Read(table, id) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          let tx = db.transaction(table).objectStore(table).get(id);
          tx.onsuccess = (e) => {
            if (request.result && e.target.result) {
              resolve(e.target.result);
            } else {
              reject(`${id} does not exist in the local data store table "${table}".`);
            }
          }
          tx.onerror = (e) => {
            reject(e);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    ReadAll(table) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          let store = db.transaction(table).objectStore(table);
          let values = [];
          store.openCursor().onsuccess = (e) => {
            var cursor = e.target.result;
            if (cursor) {
              values.push(cursor.value);
              cursor.continue();
            } else {
              resolve(values);
            }
          };
          store.openCursor().onerror = (e) => {
            reject(e.target.error);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    aUpdate(table, aData) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        let jobs = [];
        request.onsuccess = (e) => {
          let db = request.result;
          aData.forEach(data => {
            let job = this._Update(table, data, db);
            jobs.push(job);
          });
          Promise.allSettled(jobs).then(result => {
            result = result.map(e => e.status);
            resolve(e);
          }).catch(error => {
            reject(error);
          });
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Clear(table) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          let tx = db.transaction([table], "readwrite").objectStore(table).clear();
          tx.onsuccess = (e) => {
            resolve(e);
          }
          tx.onerror = (e) => {
            reject(e.target.error);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Populate(table, aData) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        let jobs = [];
        request.onsuccess = (e) => {
          let db = request.result;
          this.Clear(table).then(() => {
            aData.forEach(data => {
              let job = this._Update(table, data, db);
              jobs.push(job);
            })
          });
          Promise.allSettled(jobs).then(result => {
            result = result.map(e => e.status);
            resolve(e);
          }).catch(error => {
            reject(error);
          });
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Create(table, data) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          let tx = db.transaction([table], "readwrite").objectStore(table).add(data);
          tx.onsuccess = (e) => {
            resolve(e);
          }
          tx.onerror = (e) => {
            reject(e.target.error);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Delete(table, id) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          let tx = db.transaction([table], "readwrite").objectStore(table).delete(id);
          tx.onsuccess = (e) => {
            resolve(e);
          }
          tx.onerror = (e) => {
            reject(e.target.error);
          }
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    Update(table, object) {
      return new Promise((resolve, reject) => {
        let request = indexedDB.open("IRIS", 1);
        request.onsuccess = (e) => {
          let db = request.result;
          this._Update(table, object, db).then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        }
        request.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
    _Update(table, object, db) {
      return new Promise((resolve, reject) => {
        let tx = db.transaction([table], "readwrite").objectStore(table).put(object);
        tx.onsuccess = (e) => {
          resolve(e);
        }
        tx.onerror = (e) => {
          reject(e.target.error);
        }
      })
    }
  }
  return IndexedDB;
});
