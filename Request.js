sap.ui.define([
    "./IndexedDB",
    "./request.conf",
    "./dependencies/uuid/uuidv4.min",
    "sap/ui/model/json/JSONModel",
], function(IndexedDB, _CONF, UUID, JSONModel) {
    /* eslint-disable no-unused-vars */
    class Request {
        constructor(oController, iDatabase) {
            this.oController = oController;
            iDatabase ? this.iDatabase = iDatabase : this.iDatabase = new IndexedDB();
            this.declareProperties();
            let handler = {
                get: (target, prop, receiver) => {
                    if (!this[prop]) {
                        return (sMethod, oFilter, oParameters, _sModel) => {
                            return new Promise((resolve, reject) => {
                                this.oController.setBusy(true);
                                let sPath = `/${prop}(...)`;
                                let sModel = _sModel ? _sModel : this.getModel(prop);
                                let method;
                                if (sMethod === "GET") {
                                    method = this.GETOperation.bind(this, sModel, sPath, oParameters, oFilter);
                                } else if (sMethod === "POST") {
                                    method = this.POSTOperation.bind(this, sModel, sPath, oParameters, oFilter);
                                } else {
                                    reject(`Request does not support the ${sMethod} method. Request only enables the GET, POST methods.`)
                                }
                                method().then(response => {
                                    resolve(response);
                                }).catch(error => {
                                    console.log(error);
                                    if (error.message && error.message.includes("401")) {
                                        location.reload();
                                    } else {
                                        reject(error);
                                    }
                                }).finally(() => {
                                    this.oController.setBusy(false);
                                });
                            });
                        };
                    } else {
                        return this[prop];
                    }
                }
            };
            this.propagate();
            return new Proxy(this, handler);
        }
        declareProperties() {
            let oJSONModel = new sap.ui.model.json.JSONModel(_CONF);
            this.oController.setModel(oJSONModel, "request_conf");
            this.conf = this.oController.getModel("request_conf"); 
            this.oController.offline = false;
            this.active = null;
            if (!window.localStorage.getItem("_OFFLINE")) window.localStorage.setItem("_OFFLINE", false);
            this.dependents = {
                _ORQ1: null
            }
            this.declareUIConf();
            this.declareLazyModifier();
            let oModel = new sap.ui.model.json.JSONModel({});
            this.oController.setModel(oModel, "request");
        }
        declareLazyModifier() {
            this.lazyModifier = new Map([]);
        }
        declareUIConf() {
            this.oModalConf = new Map([
                ["Queue", {
                    title: "Queue",
                    model: "request",
                    icon: "{request>icon}",
                    text: {
                        parts: ["request>table", "request>instance/sDescriptor","request>instance/sPath","request>instance/description"],
                        formatter: (table, sDescriptor, sPath, description) => {
                            let tables = ["_Request", "_Attachments", "_Fetch"];
                            let index = tables.indexOf(table);
                            if (index === 0) {
                                return `${sDescriptor}: ${sPath}`;
                            } else if (index === 1 || index === 2) {
                                return `${description}`;
                            }
                        }
                    },
                    label: {
                        parts: ["request>table", "request>instance/sDate", "request>instance/date"],
                        formatter: (table, sDate, date) => {
                            let tables = ["_Request", "_Attachments", "_Fetch"];
                            let index = tables.indexOf(table);
                            if (index === 0) {
                                return new Date(sDate).toLocaleString("en-AU");
                            } else if (index === 1 || index === 2) {
                                return new Date(date).toLocaleString("en-AU");
                            }
                        }
                    },
                    description: "{request>description}",
                    id: "request",
                    childId: "detail"
                }],
                ["Detail", {
                    model: "request",
                    title: "Details",
                    icon: "{request>icon}",
                    text: {
                        parts: ["request>table","request>instance/sDescriptor", "request>instance/sPath", "request>instance/description"],
                        formatter: (table, sDescriptor, sPath, description) => {
                            let tables = ["_Request", "_Attachments", "_Fetch"];
                            let index = tables.indexOf(table);
                            if (index === 0) {
                                return `${sDescriptor}: ${sPath}`;
                            } else if (index === 1 || index === 2) {
                                return `${description}`;
                            }
                        }
                    },
                    state: {
                        text: {
                            path: "request>state",
                            formatter: (s) => {
                                let r = this.formatter(s)
                                if (r) return r.text;
                            }
                        },
                        color: {
                            path: "request>state",
                            formatter: (s) => {
                                let r = this.formatter(s)
                                if (r) return r.color;
                            }
                        },
                        icon: {
                            path: "request>state",
                            formatter: (s) => {
                                let r = this.formatter(s)
                                if (r) return r.icon;
                            }
                        },
                    },
                    label: {
                        parts: ["request>table", "request>instance/sDate", "request>instance/date"],
                        formatter: (table, sDate, date) => {
                            let tables = ["_Request", "_Attachments", "_Fetch"];
                            let index = tables.indexOf(table);
                            if (index === 0) {
                                return new Date(sDate).toLocaleString("en-AU");
                            } else if (index === 1 || index === 2) {
                                return new Date(date).toLocaleString("en-AU");
                            }
                        }
                    },
                    description: "{request>description}",
                    request: "{request>instance/oParameters}",
                    id: "detail",
                    parentId: "request",
                    actions: [{
                        visible: true,
                        icon: "sap-icon://delete",
                        alt: this.oController.i18n("LABEL_DELETE"),
                        callback: (e) => this.delete(e)
                    },{
                        visible: "{= ${request>state} !== 'fulfilled' && navigator.onLine }",
                        icon: "sap-icon://refresh",
                        alt: this.oController.i18n("LABEL_RETRY"),
                        callback: (e) => this.retry(e)
                    }]
                }],
                ["Message", {
                    model: "message",
                    title: "Message",
                    icon: "sap-icon://message-warning",
                    text: "{message>message}",
                    label: "{message>type}",
                    description: "{message>description}",
                    id: "message",
                    parentId: "messages",
                    actions: [{
                        icon: "sap-icon://delete",
                        label: this.oController.i18n("LABEL_DELETE"),
                        callback: (oEvent) => {
                            let oSource = oEvent.getSource();
                            let oPage = oSource.getParent().getParent()
                            let message = oPage.getBindingContext("message").getObject();
                            sap.ui.getCore().getMessageManager().removeMessages(message);
                            oPage.getParent().to("messages");
                        }
                    }]
                }],
                ["Messages", {
                    model: "message",
                    title: "Messages",
                    icon: "sap-icon://message-warning",
                    text: "{message>message}",
                    label: "{message>type}",
                    description: "{message>description}",
                    id: "messages",
                    childId: "message"
                }],
            ]);
        }
        getRoute(value) {
            let aRoutes = this.conf.getProperty("/routes");
            return aRoutes.find(e => e.path === value);
        }
        formatter(value) {
            let aFormat = this.conf.getProperty("/format");
            return aFormat.find(e => e.name === value);
        }
        template(id, setting, container) {
            let master = [
                {
                    text: "{= 'Queued (' + ${request>/}.length + ')'}",
                    label: null,
                    icon: "sap-icon://outbox",
                    id: "request",
                    callback: () => container.to("request")
                },
                {
                    text: "{= 'Messages (' + ${message>/}.length + ')'}",
                    label: null,
                    icon: "sap-icon://message-warning",
                    id: "messages",
                    callback: () => container.to("messages")
                }
            ]
            let map = new Map([
                ["master", () => {
                    return new sap.m.Page({
                        title: "Master",
                        id: "master",
                        showHeader: true,
                        customHeader: new sap.m.OverflowToolbar({
                            content: [
                                new sap.m.Title({
                                    text: "Request Manager"
                                }).addStyleClass("dxcUiGrayText"),
                                new sap.m.ToolbarSpacer(),
                                new sap.m.OverflowToolbarButton({
                                    fieldGroupIds: ["dxcIdSync"],
                                    icon: "sap-icon://synchronize",
                                    type: "Transparent",
                                    press: () => this.oController.sync()
                                })
                            ]
                        }),
                        content: new sap.m.List({
                            mode: "SingleSelectMaster",
                            rememberSelections: false,
                            selectionChange: (oEvent) => {
                                let listItem = oEvent.getParameter("listItem");
                                oEvent.getSource().setSelectedItem(listItem, false);
                                container.to(listItem.data("id"))
                            },
                            items: master.map(item => this.listTemplate(item))
                        })
                    });
                }],
                ["list", (setting) => {
                    let model = setting.model;
                    let childId = setting.childId;
                    let content = [
                        new sap.m.OverflowToolbarButton({
                            icon: "sap-icon://slim-arrow-left",
                            type: "Transparent",
                            press: () => container.backToPage("master")
                        }),
                        new sap.m.Title({
                            text: setting.title,
                        }).addStyleClass("dxcUiGrayText"),
                        new sap.m.ToolbarSpacer(),
                    ]
                    if (setting.id === "messages") {
                        content.push(new sap.m.OverflowToolbarButton({
                            icon: "sap-icon://clear-all",
                            type: "Transparent",
                            press: () => sap.ui.getCore().getMessageManager().removeAllMessages()
                        }))
                    } else if (setting.id === "request") {
                        content.push(new sap.m.OverflowToolbarButton({
                            icon: "sap-icon://clear-all",
                            type: "Transparent",
                            press: () => this.removeAllRequests()
                        }))
                        content.push(new sap.m.OverflowToolbarButton({
                            fieldGroupIds: ["dxcIdSync"],
                            icon: "sap-icon://synchronize",
                            type: "Transparent",
                            press: () => this.oController.sync()
                        }));
                    }
                    return new sap.m.Page({
                        title: setting.title,
                        id: setting.id,
                        showHeader: true,
                        customHeader: new sap.m.OverflowToolbar({
                            content: content
                        }),
                        content: new sap.m.List({
                            rememberSelections: false,
                            mode: "SingleSelectMaster",
                            selectionChange: (oEvent) => {
                                let parameter = oEvent.getParameter("listItem");
                                let page = container.getPage(childId);
                                page.bindElement({
                                    path: `${model}>${parameter.getBindingContextPath(model)}`
                                });
                                oEvent.getSource().setSelectedItem(parameter, false);
                                container.to(childId)
                            },
                            items: {
                                path: `${setting.model}>/`,
                                template: this.listTemplate(setting)
                            }
                        })
                    })
                }],
                ["detail", (setting) => {
                    return new sap.m.Page({
                        title: setting.title,
                        id: setting.id,
                        showHeader: true,
                        customHeader: new sap.m.OverflowToolbar({
                            content: [
                                new sap.m.OverflowToolbarButton({
                                    icon: "sap-icon://slim-arrow-left",
                                    type: "Transparent",
                                    press: () => container.backToPage(setting.parentId)
                                }),
                                new sap.m.Title({
                                    text: setting.title,
                                }).addStyleClass("dxcUiGrayText"),
                                new sap.m.ToolbarSpacer(),
                                new sap.m.OverflowToolbarButton({
                                    fieldGroupIds: ["dxcIdSync"],
                                    icon: "sap-icon://synchronize",
                                    type: "Transparent"
                                })
                            ]
                        }),
                        showFooter: true,
                        footer: new sap.m.OverflowToolbar({
                            content: setting.actions.map(action => {
                                return new sap.m.OverflowToolbarButton({
                                    icon: action.icon,
                                    type: "Transparent",
                                    visible: action.visible ? action.visible : true,
                                    alt: action.alt,
                                    press: action.callback
                                })
                            })
                        }),
                        content: new sap.m.HBox({
                            items: [
                                new sap.ui.core.Icon({
                                    src: setting.icon
                                }).addStyleClass("sapUiSmallMargin"),
                                new sap.m.VBox({
                                    items: [
                                        new sap.m.Title({
                                            text: setting.text,
                                            wrapping: true
                                        }),
                                        new sap.m.Label({
                                            text: setting.label,
                                            wrapping: true
                                        }),
                                        new sap.m.ObjectStatus({
                                            inverted: true,
                                            state: setting.state.color,
                                            text: setting.state.text,
                                            icon: setting.state.icon
                                        }),
                                        new sap.m.Text({
                                            text: setting.description
                                        })
                                    ]
                                })
                            ]
                        }).addStyleClass("sapUiSmallMarginTop")
                    }).addStyleClass("sapUiResponsivePadding")
                }],
                ["message", (setting) => {
                    return new sap.m.Page({
                        title: setting.title,
                        id: setting.id,
                        showHeader: true,
                        customHeader: new sap.m.OverflowToolbar({
                            content: [
                                new sap.m.OverflowToolbarButton({
                                    icon: "sap-icon://slim-arrow-left",
                                    type: "Transparent",
                                    press: () => container.backToPage(setting.parentId)
                                }),
                                new sap.m.Title({
                                    text: setting.title,
                                }).addStyleClass("dxcUiGrayText")
                            ]
                        }),
                        showFooter: true,
                        footer: new sap.m.OverflowToolbar({
                            content: setting.actions.map(action => {
                                return new sap.m.OverflowToolbarButton({
                                    icon: action.icon,
                                    type: "Transparent",
                                    visible: action.visible ? action.visible : true,
                                    alt: action.alt,
                                    press: action.callback
                                })
                            })
                        }),
                        content: new sap.m.HBox({
                            items: [
                                new sap.ui.core.Icon({
                                    src: setting.icon
                                }).addStyleClass("sapUiSmallMargin"),
                                new sap.m.VBox({
                                    items: [
                                        new sap.m.Title({
                                            text: setting.text,
                                            wrapping: true
                                        }),
                                        new sap.m.Label({
                                            text: setting.label,
                                            wrapping: true
                                        }),
                                        new sap.m.Text({
                                            text: setting.description
                                        }),
                                    ]
                                })
                            ]
                        }).addStyleClass("sapUiSmallMarginTop")
                    }).addStyleClass("sapUiResponsivePadding")
                }],
            ])
            return map.get(id)(setting);
        }
        listTemplate(setting) {
            let items = [new sap.m.Text({
                text: setting.text
            })]
            if (setting.label) items.push(new sap.m.Label({
                text: setting.label
            }).addStyleClass("dxcUiLabel"));
            return new sap.m.CustomListItem({
                customData: [
                    new sap.ui.core.CustomData({
                        key: "id",
                        value: setting.id
                    })
                ],
                content: [new sap.m.HBox({
                    alignItems: "Center",
                    items: [new sap.ui.core.Icon({
                        src: setting.icon
                    }).addStyleClass("sapUiSmallMargin"),
                    new sap.m.VBox({
                        items: items
                    }).addStyleClass("sapUiSmallMarginTopBottom sapUiLargeMarginEnd")]
                })],
            }).addStyleClass("dxcUiRequest sapUiResponsivePadding");
        }
        changeTab(oEvent, list) {
            var oParameters = oEvent.getParameters();
            let tabKey = oParameters.item.getKey();
            this.activeTab = tabKey;
            let setting = this.oModalConf.get(tabKey);
            list.bindAggregation("items", {
                path: `${setting.model}>/`,
                templateShareable: true,
                template: this.listTemplate(setting),
            });
        }
        factory(sId, oContext) {
            let setting = this.oModalConf.get("Queue");
            return this.listTemplate(setting).clone(sId);
        }
        render(oEvent) {
            let oSource = oEvent.getSource();
            this.propagate().then(() => {
                let container = new sap.m.NavContainer({
                    initialPage: "master"
                });
                if (!this.dependents._ORQ1) {
                    let pages = [
                        this.template("master", null, container),
                        this.template("list", this.oModalConf.get("Queue"), container),
                        this.template("list", this.oModalConf.get("Messages"), container),
                        this.template("detail", this.oModalConf.get("Detail"), container),
                        this.template("message", this.oModalConf.get("Message"), container)
                    ];
                    pages.forEach(page => container.insertPage(page));
                    this.dependents._ORQ1 = new sap.m.Popover({
                        title: this.oController.i18n("Messages"),
                        showHeader: false,
                        placement: "Bottom",
                        contentMinWidth: "280px",
                        contentHeight: "500px",
                        content: container,
                        afterClose: (oEvent) => {
                            let source = oEvent.getSource();
                            source.destroyContent();
                            source.destroy();
                            this.dependents._ORQ1 = null;   
                        }
                    }).addStyleClass("");
                    this.oController.getView().addDependent(this.dependents._ORQ1);
                }
                this.dependents._ORQ1.openBy(oSource); 
            })
        }
        propagate() {
            return new Promise(async (resolve, reject) => {
                this.iDatabase.ReadAll("Request").then(data => {
                    let oModel = new sap.ui.model.json.JSONModel(data);
                    this.oController.setModel(oModel, "request");
                    resolve();
                }).catch(error => {
                    console.warn(error);
                    reject(error);
                })
            })
        }
        _propagate() {
            return new Promise(async (resolve, reject) => {
                this.iDatabase.ReadAll("_Request").then(data => {
                    for (let elem of data) elem.bActive = this.active === elem.id;
                    let oModel = new sap.ui.model.json.JSONModel(data);
                    this.oController.setModel(oModel, "request");
                    return this.iDatabase.ReadAll("_Attachments");
                }).then(data => {
                    let oModel = new sap.ui.model.json.JSONModel(data);
                    this.oController.setModel(oModel, "file");
                    return this.iDatabase.ReadAll("_Fetch");
                }).then(data => {
                    let oModel = new sap.ui.model.json.JSONModel(data);
                    this.oController.setModel(oModel, "fetch");
                }).finally(() => {
                    resolve();
                }).catch(error => {
                    console.warn(error);
                    reject(error);
                })
            })
        }
        flagActive(id) {
            let model = this.oController.getModel("request");
            let data = model.getData();
            let prev = data.find(e => e.bActive);
            if (prev) prev.bActive = false;
            let property = data.find(e => e.id === id);
            if (property) {
                property.bActive = true;
                this.active = id;
            }
            this.oController.setModel(new sap.ui.model.json.JSONModel(data), "request");
        } 
        commitGhostMode(b) {
            window.localStorage.setItem("_OFFLINE", b);
        }
        reverseGhostMode() {
            let offline = JSON.parse(window.localStorage.getItem("_OFFLINE"));
            window.localStorage.setItem("_OFFLINE", !offline);
        }
        isGhostMode() {
            return JSON.parse(window.localStorage.getItem("_OFFLINE"));
        }
        _oFilterTosFilter(oFilter) {
            var sRes;
            if (!oFilter) {
                return "";
            }
            if (oFilter._bMultiFilter) {
                sRes = "";
                var bAnd = oFilter.bAnd;
                oFilter.aFilters.forEach(function(oFilter, index, aFilters) {
                    sRes += this._oFilterTosFilter(oFilter);
                    if (aFilters.length - 1 != index) {
                        sRes += bAnd ? " and " : " or ";
                    }
                }, this);
                return sRes;
            } else if (oFilter.filters) {
                sRes = "";
                var bAnd = oFilter.and;
                oFilter.filters.forEach(function(oFilter, index, aFilters) {
                    sRes += this._oFilterTosFilter(oFilter);
                    if (aFilters.length - 1 != index) {
                        sRes += bAnd ? " and " : " or ";
                    }
                }, this);
                return sRes;
            } else {
                if (oFilter.sOperator === sap.ui.model.FilterOperator.Any || oFilter.sOperator === sap.ui.model.FilterOperator.All) {
                    sRes = oFilter.sPath + " " + oFilter.sOperator.toLowerCase() + " " + this._oFilterTosFilter(oFilter.oCondition);
                } else {
                    sRes = oFilter.sPath + " " + oFilter.sOperator.toLowerCase() + " '" + oFilter.oValue1 + "'";
                    if (oFilter.sOperator === sap.ui.model.FilterOperator.BT) {
                        sRes += "...'" + oFilter.oValue2 + "'";
                    }
                }
                return sRes;
            }
        }

        getParameters(e) {
            let target = e.oParameters;
            let oRoute = this.getRoute(e.sPath);
            if (oRoute) {
                let definition = oRoute;
                if (definition.property) {
                    target = {};
                    target[definition.property] = this.copy(e.oParameters);
                }
            }
            return target;
        }
        delete(oEvent, table = "_Request", model = "request") {
            let page = oEvent.getSource().getParent().getParent();
            let data = page.getBindingContext(model).getObject();
            let transient = this.iDatabase.Read(table, data.instance.id);
            if (transient) {
                sap.m.MessageBox.warning(this.oController.i18n("WARN_RLDEL"), {
                    actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CLOSE],
                    emphasizedAction: sap.m.MessageBox.Action.OK,
                    onClose: (sAction) => {
                        if (sAction === sap.m.MessageBox.Action.OK) {
                            this.iDatabase.Delete("Request", data.id);
                            this.iDatabase.Delete(table, data.instance.id);
                            this.propagate();
                        }
                    }
                });
            } else {
                this.iDatabase.Delete("Request", data.id);
                this.propagate();
            }
        }
        retry(oEvent, table = "_Request", model = "request") {
            if (navigator.onLine && !this.isGhostMode()) {
                let control = oEvent.getSource().getParent().getParent();
                let container = control.getParent();
                let parent = control.getBindingContext(model).getObject();
                let instance = parent.instance;
                this.webRequest(instance.sModel, instance.sPath, instance.oParameters, instance.oFilter).then(data => {
                    this.propagate();
                    container.backToPage("request");
                }).catch(async error => {
                    await this.notify({
                        description: error,
                        state: "rejected",
                        icon: "sap-icon://message-warning"
                    }, instance.id);
                });
            } else {
                sap.m.MessageToast.show("Work Order Manager was unable to retry this Request.", {
                    my: "right top",
                    at: "right top"
                })
            }
        }
        _delete(oEvent, table = "_Request", model = "request") {
            if (this.activeTab) {
                let active = this.oModalConf.get(this.activeTab);
                table = active.table;
                model = active.model;
            }
            let oParameters = oEvent.getParameters();
            console.log(oParameters, oEvent.getSource());
            sap.m.MessageBox.warning(this.oController.i18n("WARN_RLDEL"), {
                actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CLOSE],
                emphasizedAction: sap.m.MessageBox.Action.OK,
                onClose: function (sAction) {
                    if (sAction === sap.m.MessageBox.Action.OK) {
                        let listItem = oParameters.listItem;
                        let data = listItem.getBindingContext(model).getObject();
                        this.iDatabase.Delete(table, data.id);
                        this.propagate();
                    }
                }
            });
        }

        removeAllRequests() {
            this.iDatabase.ReadSome("Request", {
                sOperator: "EQ",
                oValue1: "fulfilled",
                sPath: "state"
            }).then(instances => {
                instances.forEach(instance => {
                    this.iDatabase.Delete("Request", instance.id);
                });
                this.propagate();
            });
        }
        
        async notify(update, id) {
            return new Promise((resolve, reject) => {
                let execute = (data) => {
                    this.iDatabase.Update("Request", data).then(response => {
                        this.propagate();
                        resolve(response);
                    }).catch(error => {
                        reject(error);
                    })
                }
                if (id) {
                    this.iDatabase.ReadSome("Request", {
                        sOperator: "EQ",
                        oValue1: id,
                        sPath: "instanceId"
                    }).then(instance => {
                        if (instance.length > 0) {
                            instance = instance[0];
                            let keys = Object.keys(instance);
                            keys.forEach(key => {
                                if (update[key]) instance[key] = update[key];
                            });
                            execute(instance);
                        } else {
                            reject(`Request.js was unable to locate a notify entry in the Request IndexedDB table that had a matching instance id of: ${id}`)
                        }
                    });
                } else if (update) {
                    update.id = uuidv4();
                    execute(update);
                }
                this.propagate();
            });
        }

        execute() {
            return new Promise((resolve, reject) => {
                let err = [];
                this.iDatabase.ReadAll("_Request").then(async rows => {
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                        this.flagActive(row.id);
                        this.notify({
                            state: "synchronize",
                            icon: "sap-icon://synchronize"
                        }, row.id)
                        let response = await this.webRequest(row.sModel, row.sPath, this.getParameters(row), row.oFilter).catch(error => err.push(error));
                        if (response) {
                            this.iDatabase.Delete("_Request", row.id);
                        }
                        this.notify({
                            state: response ? "fulfilled" : "rejected",
                            icon: response ? "sap-icon://task" : "sap-icon://message-warning"
                        }, row.id)
                    }
                    this.flagActive(null);
                    return this.iDatabase.ReadAll("Batch");
                }).then(async rows => {
                    for (var i = 0; i < rows.length; i++) {
                        var row = rows[i];
                        this.flagActive(row.id);
                        this.notify({
                            state: "synchronize",
                            icon: "sap-icon://synchronize"
                        }, row.id)
                        let response = await this.webRequest(row.sModel, row.sPath, this.getParameters(row), row.oFilter).catch(error => err.push(error));
                        if (response) {
                            this.iDatabase.Delete("Batch", row.id);
                        }
                        this.notify({
                            state: response ? "fulfilled" : "rejected",
                            icon: response ? "sap-icon://task" : "sap-icon://message-warning"
                        }, row.id)
                    }
                    this.flagActive(null);
                    return this.iDatabase.ReadAll("_Attachments");
                }).then(async rows => {
                    for (var i = 0; i < rows.length; i++) {
                        this.notify({
                            state: "synchronize",
                            icon: "sap-icon://synchronize"
                        }, rows[i].id)
                        let response = await this.oController.filer.xUpload(rows[i].file, rows[i].descriptor).catch(error => err.push(error));
                        if (response) {
                            this.iDatabase.Delete("_Attachments", rows[i].id);
                        }
                        this.notify({
                            state: response ? "fulfilled" : "rejected",
                            icon: response ? "sap-icon://task" : "sap-icon://message-warning"
                        }, rows[i].id)
                    }
                    return this.iDatabase.ReadAll("_Fetch");
                }).then(async rows => {
                    for (var i = 0, row = rows[i]; i < rows.length; i++) {
                        this.notify({
                            state: "synchronize",
                            icon: "sap-icon://synchronize"
                        }, rows[i].id)
                        let response = await fetch(row.url, {
                            body: row.body,
                            headers: new Headers(row.headers),
                            method: row.method
                        }).catch(error => err.push(error));
                        this.iDatabase.Delete("_Fetch", rows[i].id);
                        if (response.ok) {
                            return response.text();
                        } else {
                            err.push(new Error(response.statusText));
                        }
                        this.notify({
                            state: response ? "fulfilled" : "rejected",
                            icon: response.ok ? "sap-icon://task" : "sap-icon://message-warning"
                        }, rows[i].id)
                    }
                    resolve();
                }).catch(error => {
                    reject(error);
                }).finally(() => {
                    err.forEach(e => this.newMessage({
                        message: this.oController.i18n("ERR_SYNC"),
                        description: e,
                        type: "Error",
                    }))
                });
            })
        }

        getRequests() {
            return this.iDatabase.ReadAll("_Request");
        }

        deleteRequest(id) {
            return this.iDatabase.Delete("_Request", id);
        }

        getFields(data) {
            let definition = [{
                path: "WO_Status.UserStatus",
                map: ["OperationStatusID", "OperationStatusDescription"],
            }]
            return new Promise(resolve => {
                data = data.operation ? data.operation : data;
                for (let def of definition) {
                    var path = def.path;
                    var temp = this.copy(data);
                    for (var i = 0, path = path.split("."), length = path.length; i < length; i++) {
                        temp = temp[path[i]];
                    }
                    def.map.forEach(field => data[field] = temp);
                }
                resolve(data);
            })
        }

        transience(aTable, sId, oParameters) {
            return new Promise((resolve, reject) => {
                if (!Array.isArray(aTable)) aTable = [aTable];
                Promise.all(aTable.map(sTable => this.iDatabase.Read(sTable, sId))).then(async (aResults) => {
                    for (var nIndex in aResults) {
                        let oResult = aResults[nIndex];
                        let sTable = aTable[nIndex];
                        if (oResult) {
                            let target = {
                                id: sId
                            }
                            oParameters = await this.getFields(oParameters);
                            Object.assign(target, oParameters);
                            this.iDatabase.Update(sTable, target);
                        }
                    }
                    resolve();
                }).catch(error => {
                    reject(error);
                });
            })
        }

        onBeforeWriteParameters(sKey, sType, _oParameters, oParameters) {
            switch (sType) {
                case "Array": 
                    if (!_oParameters[sKey]) _oParameters[sKey] = [];
                    if (!Array.isArray(oParameters)) oParameters = [oParameters];
                    oParameters.forEach(e => _oParameters[sKey].push(e));
                    break;
            }
            return _oParameters;
        }

        async onBeforePOSTOperation(oItem, oParameters) {
            return new Promise(async (resolve, reject) => {
                if (oItem.format) {
                    let sKey = Object.keys(oItem.format)[0];
                    let sType = oItem.format[sKey].name;
                    let oFilter = {
                        sPath: "sPath",
                        sOperator: "EQ",
                        oValue1: oItem.path
                    }
                    let aResult = await this.iDatabase.ReadSome("_Request", oFilter).catch(error => reject(error));
                    let uuid;
                    let _oParameters;
                    if (aResult.length > 0) {
                        let oResult = aResult[0];                    
                        _oParameters = oResult.oParameters;
                        uuid = oResult.id;
                    } else {
                        _oParameters = {};
                        uuid = uuidv4();
                    }
                    _oParameters = this.onBeforeWriteParameters(sKey, sType, _oParameters, oParameters);
                    resolve({ uuid, _oParameters });
                } else {
                    let uuid = uuidv4();
                    resolve({ uuid, oParameters });
                }
            })
        }

        POSTOperation(sModel, sPath, oParameters, oFilter) {
            return new Promise(async (resolve, reject) => {
                let item = this.getRoute(sPath);
                let id = this.createId(oParameters, item.id);
                if (navigator.onLine && !this.isGhostMode()) {
                    this.webRequest(sModel, sPath, oParameters, oFilter).then(data => {
                        resolve(data);
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    await this.transience(item.table, id, oParameters);
                    let { uuid, _oParameters } = await this.onBeforePOSTOperation(item, oParameters);
                    let data = {
                        id: uuid,
                        sModel: sModel,
                        sPath: sPath,
                        oParameters: _oParameters,
                        oFilter: oFilter,
                        sDescriptor: `${item.text.descriptor} ${item.text.singleton}`,
                        sDate: new Date()
                    }
                    Promise.all([
                        this.iDatabase.Update("_Request", data),
                        this.notify({
                            id: uuidv4(),
                            instanceId: uuid,
                            table: "_Request",
                            icon: "sap-icon://outbox",
                            instance: data,
                            description: id,
                            state: "queued"
                        })
                    ]).then(data => {
                        this.propagate();
                        resolve(data[0]);
                    }).catch(error => {
                        reject(error);
                    });
                }
            })
        }

        oParametersToFilters(oParameters, oFilter) {
            if (oParameters && Object.keys(oParameters).length > 0 && oFilter) {
                let oParametersToFilters = Object.keys(oParameters).map(sKey => {
                    return {
                        sOperator: "EQ",
                        oValue1: oParameters[sKey],
                        oValue2: null,
                        sPath: sKey
                    }
                });
                if (oFilter.aFilters) {
                    oFilter.aFilters = oFilter.aFilters.concat(oParametersToFilters);
                    return oFilter;
                } else if (oFilter.filters) {
                    oFilter.filters = oFilter.filters.concat(oParametersToFilters);
                    return oFilter;
                } else if (typeof oFilter === "object") {
                    oParametersToFilters.push(oFilter);
                    return new sap.ui.model.Filter({
                        filters: oParametersToFilters,
                        and: true
                    });
                } else {
                    return new sap.ui.model.Filter({
                        filters: oParametersToFilters,
                        and: true
                    });
                }
            }
        }

        GETOperation(sModel, sPath, oParameters, oFilter) {
            return new Promise((resolve, reject) => {
                let promise = null;
                let oBinding = this.getRoute(sPath);
                if (navigator.onLine && !this.isGhostMode()) {
                    promise = this.webRequest(sModel, sPath, oParameters, oFilter);
                } else if (oBinding) {
                    let table = oBinding.table;
                    if (oParameters) oFilter = this.oParametersToFilters(oParameters, oFilter);
                    if (oFilter) {
                        promise = this.readSome(table, oFilter);
                    } else {
                        promise = this.readAll(table);
                    }
                }
                if (promise) {
                    promise.then(data => {
                        resolve(data);
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    reject(new Error("Device does not have a working Internet connection and this service does not have an offline feature. "));
                }
            });
        }
        copy(data) {
            return JSON.parse(JSON.stringify(data));
        }
        createId(value, paths) {
            if (!Array.isArray(paths)) paths = [paths];
            let values = [];
            paths.forEach(path => {
                let temp = this.copy(value);
                for (var i = 0, path = path.split("."), length = path.length; i < length; i++) {
                    temp = temp[path[i]];
                }
                values.push(temp);
            });
            let id = values.join("-");
            return id ? id : uuidv4();
        }
        rules(table, results, oFilter) {
            let dictionary = [
                {
                    condition: table === "OrdersSAP" && results.length === 1 && oFilter && oFilter.aFilters && oFilter.aFilters.some(e => ["WorkOrderNumber", "OperationNumber"].includes(e.sPath)),
                    callback: () => {
                        this.iDatabase.Update(table, results[0]);
                    }
                },
                {
                    condition: table === "OrdersSAP" && results.length === 1 && oFilter && oFilter.filters && oFilter.filters.some(e => ["WorkOrderNumber", "OperationNumber"].includes(e.sPath)),
                    callback: () => {
                        this.iDatabase.Update(table, results[0]);
                    }
                }
            ]
            let match = dictionary.find(e => e.condition);
            return match ? match.callback : false;
        }
        webRequest(sModel, sPath, oParameters, oFilter) {
            return new Promise((resolve, reject) => {
                let oModel = this.oController.getModel(sModel);
                let oBindingContext = oModel.createBindingContext("/");
                let oOperation = oModel.bindContext(sPath, oBindingContext);
                let _def = this.getRoute(sPath);
                if (oFilter && (!oParameters || !oParameters.$filter)) {
                    let sFilter = this._oFilterTosFilter(oFilter);
                    oOperation.changeParameters({
                        $filter: sFilter
                    })
                }
                if (oParameters) {
                    if (oParameters.id) delete oParameters.id;
                    if (_def && _def.format) {
                        let sKey = Object.keys(_def.format)[0];
                        let sType = _def.format[sKey].name;
                        oParameters = this.onBeforeWriteParameters(sKey, sType, {}, oParameters);
                    }
                    this.kvifyParameters(oParameters).forEach(e => oOperation.setParameter(e.k, e.v));
                }
                oOperation.execute().then(() => {
                    let response = oOperation.getBoundContext().getObject();
                    let results = response.value ? response.value : response;
                    if (_def && results && Object.keys(results).length > 0) {
                        if (!Array.isArray(results)) results = [results];
                        let insert = [];
                        results.forEach(async e => {
                            e.id = this.createId(e, _def.id);
                            if (e.type && e.type === "error") {
                                await this.notify({
                                    description: e.messageJSON ? e.messageJSON : "Unexpected error",
                                    state: "rejected",
                                    icon: "sap-icon://message-warning"
                                }, e.id);
                            } else {
                                insert.push(e);
                            }
                        });
                        let conditional = this.rules(_def.table, results, oFilter);
                        conditional ? conditional() : this.iDatabase.Populate(_def.table, insert);
                    }
                    resolve(results);
                }).catch((error) => {
                    try {
                        let message = JSON.parse(error.message);
                        let format = `${message.where}: ${message.message}`;
                        if (message.statusCode === 502 || message.message.includes("502")) message.message = this.oController.i18n("ERR_502");
                        reject(format);
                    } catch (e) {
                        reject(error);
                    }
                });
            });
        }
        readAll(table) {
            return new Promise((resolve, reject) => {
                this.iDatabase.ReadAll(table).then(data => {
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
        readSome(table, oFilter) {
            return new Promise((resolve, reject) => {
                this.iDatabase.ReadSome(table, oFilter).then(data => {
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
        getModel(prop) {
            let exp = /^GET[A-Z0-9\_\-]+/gi;
            let func = exp.test(prop);
            let oModels = this.conf.getProperty("/models");
            return oModels[func ? "read" : "write"]
        }
        kvifyParameters(oParameters) {
            let res = [];
            if (oParameters) {
                let keys = Object.keys(oParameters);
                for (let name of keys) {
                    res.push({
                        k: name,
                        v: oParameters[name]
                    });
                }
            }
            return res;
        }
        _sFilter(filter) {
            return new Promise((resolve, reject) => {
                if (typeof filter === Array) {
                    let sFilters = "";
                    for (var i = 0; i < filter.length; i++) {
                        let e = filter[i];
                        if (typeof filter === Object && typeof filter.property === String && typeof filter.operator === String && typeof filter.value === String) {
                            sFilters += `${e.property} ${e.operator} ${e.value}`;
                            if (i > 0 && i !== filter.length - 1) sFilters += " and ";
                        }
                    }
                    resolve(sFilters);
                } else if (typeof filter === Object && typeof filter.property === String && typeof filter.operator === String && typeof filter.value === String) {
                    resolve(`${filter.property} ${filter.operator} ${filter.value}`);
                } else {
                    reject(new Error("Illegal Argument: Argument filter must equal typeof Object and comprise the keys property, operator and value."));
                }
            });
        }
    }
    return Request;
});