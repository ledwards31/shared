sap.ui.define([], function () {
    return {
        "models": {
            "read": "wom",
            "write": "wom"
        },
        "routes": [
            {
                "path": "/getWOList(...)",
                "table": "OrdersList",
                "method": "GET",
                "id": ["WorkOrderNumber", "OperationNumber"],
                "text": {
                    "descriptor": "Fetch",
                    "singleton": "Work Orders"
                }
            },
            {
                "path": "/getWODetail(...)",
                "table": "Details",
                "method": "GET",
                "id": ["WorkOrderNumber", "OperationNumber"],
                "text": {
                    "descriptor": "Fetch",
                    "singleton": "Work Order Details"
                }
            },
            {
                "path": "/updateWorkOrder(...)",
                "table": ["OrdersList", "Details"],
                "method": "POST",
                "format": { "operation": Array },
                "id": ["WorkOrderNumber", "OperationNumber"],
                "text": {
                    "descriptor": "Modify",
                    "singleton": "Work Order"
                }
            },
            {
                "path": "/getLoggedInUserInfo(...)",
                "table": "Users",
                "method": "GET",
                "id": "",
                "text": {
                    "descriptor": "Fetch",
                    "singleton": "User Information"
                }
            },
            {
                "path": "/checkUser(...)",
                "table": "UserMeta",
                "method": "GET",
                "id": "",
                "text": {
                    "descriptor": "Fetch",
                    "singleton": "User Logon"
                }
            },
            {
                "path": "/getUserPreference(...)",
                "table": "UserPreferences",
                "method": "GET",
                "id": "",
                "text": {
                    "descriptor": "Fetch",
                    "singleton": "User Information"
                }
            }
        ],
        "format": [
            {
                "name": "fulfilled",
                "text": "Fulfilled",
                "color": "Success",
                "icon": "sap-icon://status-completed"
            },
            {
                "name": "rejected",
                "text": "Rejected",
                "color": "Error",
                "icon": "sap-icon://status-error"
            },
            {
                "name": "Queued",
                "text": "Fulfilled",
                "color": "Information",
                "icon": "sap-icon://status-inactive"
            },
            {
                "name": "synchronize",
                "text": "Synchronizing",
                "color": "Indication03",
                "icon": "sap-icon://status-in-progress"
            }
        ]
    }
});